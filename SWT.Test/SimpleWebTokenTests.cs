﻿using System;
using System.Text;
using Xunit;

namespace SWT.Test
{
    public class SimpleWebTokenTests
    {
        static readonly string Key = "N4QeKa3c062VBjnVK6fb+rnwURkcwGXh7EoNK34n0uM=";

        private static SimpleWebToken BuildToken()
        {
            var token = new TokenValues("issuer.example.com", new DateTime(2010, 1, 1))
            .Set("over18", "true")
            .Set("com.example.group", "gold")
            .Sign(Key);

            return token;
        }

        [Fact]
        public void Verify_passes_good_value()
        {
            var token = new SimpleWebToken("Issuer=issuer.example.com&ExpiresOn=1262304000&com.example.group=gold&over18=true&HMACSHA256=AT55%2B2jLQeuigpg0xm%2Fvn7tjpSGXBUfFe0UXb0%2F9opE%3D");

            Assert.True(token.Verify(Key));
        }

        [Fact]
        public void Values_survive_round_trip()
        {
            var token = new SimpleWebToken("Issuer=issuer.example.com&ExpiresOn=1262304000&com.example.group=gold&over18=true&HMACSHA256=AT55%2B2jLQeuigpg0xm%2Fvn7tjpSGXBUfFe0UXb0%2F9opE%3D");

            var values = TokenValues.ReadValues(token, Key);

            Assert.Equal("issuer.example.com", values.Issuer);
            Assert.Equal(new DateTime(2010, 1, 1), values.ExpiresOn);
            Assert.Equal("gold", values.Get("com.example.group"));
            Assert.Equal("true", values.Get("over18"));
        }

        [Fact]
        public void Verify_fails_bad_value()
        {
            var token = new SimpleWebToken("Issuer=other.example.com&ExpiresOn=1262304000&com.example.group=gold&over18=true&HMACSHA256=AT55%2B2jLQeuigpg0xm%2Fvn7tjpSGXBUfFe0UXb0%2F9opE%3D");

            Assert.False(token.Verify(Key));
        }

        [Fact]
        public void ReadValues_throws_if_sig_fails()
        {
            var token = new SimpleWebToken("Issuer=other.example.com&ExpiresOn=1262304000&com.example.group=gold&over18=true&HMACSHA256=AT55%2B2jLQeuigpg0xm%2Fvn7tjpSGXBUfFe0UXb0%2F9opE%3D");

            Assert.Throws<TokenVerificationException>(() => TokenValues.ReadValues(token, Key));
        }

        [Fact]
        public void Passes_with_ACS_generated_token()
        {
            var raw = Convert.FromBase64String("aHR0cCUzYSUyZiUyZnNjaGVtYXMueG1sc29hcC5vcmclMmZ3cyUyZjIwMDUlMmYwNSUyZmlkZW50aXR5JTJmY2xhaW1zJTJmbmFtZWlkZW50aWZpZXI9JTJicldkbU9xb2I0dTJGSW42NSUyYkVXbmpoa1JONDVFMEtQZmNXZSUyZkxSeW1NRSUzZCZodHRwJTNhJTJmJTJmc2NoZW1hcy5taWNyb3NvZnQuY29tJTJmYWNjZXNzY29udHJvbHNlcnZpY2UlMmYyMDEwJTJmMDclMmZjbGFpbXMlMmZpZGVudGl0eXByb3ZpZGVyPXVyaSUzYVdpbmRvd3NMaXZlSUQmQXVkaWVuY2U9dXJuJTNhZXRobG9jYWwuYmNjLmNvbS5hdSZFeHBpcmVzT249MTQxNjM0NzQ0MyZJc3N1ZXI9aHR0cHMlM2ElMmYlMmZldGhsb2NhbC5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0JTJmJkhNQUNTSEEyNTY9T2ZEdnBncSUyYmNRc2dPSFNJSGxzZXdGMkVHSUxCajRYJTJic1klMmJEeDclMmJMTXFBJTNk");
            var token = new SimpleWebToken(Encoding.ASCII.GetString(raw));

            Assert.True(token.Verify("pQoH8hKH1mleWfPxQ/3GD5E6BCXyadJPZnn4N30sckg="));
        }
    }
}
