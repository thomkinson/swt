﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SWT
{
    public struct SimpleWebToken
    {
        private readonly string value;

        public string Value { get { return value; } }

        public SimpleWebToken(string value)
        {
            this.value = value;
        }

        public bool Verify(string key, out string payload)
        {
            const string splitter = "&HMACSHA256=";
            var index = value.IndexOf(splitter);

            payload = value.Substring(0, index);
            var hash = Uri.UnescapeDataString(value.Substring(index + splitter.Length));
            return hash == ComputeHash(key, payload);
        }

        public bool Verify(string key)
        {
            string payload;
            return Verify(key, out payload);
        }

        public static string ComputeHash(string key, string payload)
        {
            using (var hmac = new HMACSHA256(Convert.FromBase64String(key)))
                return Convert.ToBase64String(hmac.ComputeHash(Encoding.ASCII.GetBytes(payload)));
        }
    }
}
