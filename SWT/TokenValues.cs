﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SWT
{
    public class TokenValues
    {
        public TokenValues(string issuer, DateTime expiresOn)
        {
            Issuer = issuer;
            ExpiresOn = expiresOn;
        }

        private TokenValues(Dictionary<string, string> values)
        {
            this.values = values;
        }

        private readonly Dictionary<string, string> values
            = new Dictionary<string, string>();

        public string Issuer
        {
            get { return Get("Issuer"); }
            set { Set("Issuer", value); }
        }

        public DateTime ExpiresOn
        {
            get { return GetDateTime("ExpiresOn"); }
            set { Set("ExpiresOn", value); }
        }

        public TokenValues Set(string key, string value)
        {
            values[key] = value;

            return this;
        }

        public TokenValues Set(string key, DateTime value)
        {
            return Set(key, ((int)(value - new DateTime(1970, 1, 1)).TotalSeconds).ToString());
        }

        public string Get(string key)
        {
            return values[key];
        }

        public DateTime GetDateTime(string key)
        {
            return new DateTime(1970, 1, 1).AddSeconds(Int32.Parse(values[key]));
        }

        public SimpleWebToken Sign(string key)
        {
            var payload = String.Join("&", values.Select(
                pair => String.Format("{0}={1}", Uri.EscapeDataString(pair.Key), Uri.EscapeDataString(pair.Value))));

            var hash = SimpleWebToken.ComputeHash(key, payload);
            return new SimpleWebToken(String.Format("{0}&HMACSHA256={1}", payload, Uri.EscapeDataString(hash)));
        }

        public static TokenValues ReadValues(SimpleWebToken token, string key)
        {
            string payload;
            if (!token.Verify(key, out payload))
                throw new TokenVerificationException();

            var values = payload
                .Split('&')
                .Select(chunk => chunk.Split('='))
                .ToDictionary(pair => Uri.UnescapeDataString(pair[0]), pair => Uri.UnescapeDataString(pair[1]));

            return new TokenValues(values);
        }
    }
}
